from django.db import models
import jwt


# Create your models here.
class User(models.Model):

    class Meta:

        db_table = 'user'

    email = models.CharField(max_length=256)
    password = models.CharField(max_length=32)

    def __str__(self):
        return self.email