from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token
from . import views

urlpatterns = [
    url(r'^users/$', views.MusicList.as_view(), name='music-list'),
    url(r'^login/', obtain_jwt_token)

]